# frozen_string_literal: true

require_relative 'lib/wonder_scrape/version'

Gem::Specification.new do |spec|
  spec.name          = 'wonder_scrape'
  spec.version       = WonderScrape::VERSION
  spec.authors       = ['Ben Dawson']
  spec.email         = ['bendawson.rb@gmail.com']

  spec.summary       = 'A project to collect useful information from figure collecting websites.'
  spec.homepage      = 'https://gitlab.com/maleckai/wonder_scrape'
  spec.license       = 'MIT'
  spec.required_ruby_version = Gem::Requirement.new('>= 2.3.0')

  spec.metadata['homepage_uri'] = spec.homepage
  spec.metadata['source_code_uri'] = spec.homepage
  spec.metadata['changelog_uri'] = "#{spec.homepage}/-/blob/master/CHANGELOG.md"
  spec.required_ruby_version = Gem::Requirement.new('>= 2.3.0')

  spec.metadata['allowed_push_host'] = 'https://rubygems.org'

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files = Dir.chdir(File.expand_path(__dir__)) do
    `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  end
  spec.bindir        = 'exe'
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ['lib']

  spec.add_dependency 'thor'
  spec.add_dependency 'tty-progressbar'
  spec.add_dependency 'tty-prompt'

  spec.add_runtime_dependency 'nokogiri', ['~> 1.10.9']
  spec.add_runtime_dependency 'upton', ['~> 0.3.6']
end
