# frozen_string_literal: true

require 'wonder_scrape/version'

module WonderScrape
  class Error < StandardError; end
end
