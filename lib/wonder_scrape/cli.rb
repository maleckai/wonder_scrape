# frozen_string_literal: true

require 'thor'
require_relative 'commands/scrape'

module WonderScrape
  # Handle the application command line parsing
  # and the dispatch to various command objects
  #
  # @api public
  class CLI < Thor
    # Error raised by this runner
    Error = Class.new(StandardError)

    desc 'version', 'wonder_scrape version'
    def version
      require_relative 'version'
      puts "v#{WonderScrape::VERSION}"
    end
    map %w[--version -v] => :version

    desc 'scrape', 'Scrape a target website for item data'
    method_option :target, aliases: '-t', type: :string, banner: 'targetWebsite',
                           desc: 'Sets the target website for scraping.',
                           enum: WonderScrape::Commands::Scrape::VALID_SCRAPER_NAMES
    method_option :output, aliases: '-o', type: :string, banner: 'csv',
                           desc: 'Specifies the output format',
                           enum: %w[csv json]
    method_option :file, aliases: '-f', type: :string, banner: 'path/to/file',
                         desc: 'Path to the file to write output to. Only necessary for CSV.'
    method_option :num_pages, aliases: '-n', type: :numeric, banner: 2,
                              desc: 'Expected number of pages for search results.'
    method_option :start_page, aliases: '-s', type: :numeric, banner: 1,
                               desc: 'What page of search results to begin scraping from.'
    method_option :request_delay, aliases: '-d', type: :numeric, banner: 5,
                                  desc: 'How long in seconds to wait between requests. Useful to avoid tripping rate limits.'
    method_option :verbose, aliases: '-v', type: :boolean,
                            desc: 'Runs in verbose mode, outputting in greater detail'
    method_option :help, aliases: '-h', type: :boolean,
                         desc: 'Display usage information'
    def scrape(*)
      if options[:help]
        invoke :help, ['scrape']
      else
        WonderScrape::Commands::Scrape.new(options).execute
      end
    end
  end
end
