# frozen_string_literal: true

require 'json'

class WonderScrape::Recorder
  def initialize(output, options = {})
    @output = output
    @verbose = options[:verbose] || false
    @progress_bar = options[:progress_bar]
    @items_scraped = 0
    @item_issues = {}
    @unexpected_fields = []
  end

  def print
    output.puts "Successfully processed #{items_scraped} items!"

    if unexpected_fields.count > 0
      output.puts "Encountered the following unexpected fields: #{unexpected_fields}"
    end

    if item_issues.count > 0
      output.puts "Had issues with #{item_issues.count} items below"
      output.puts JSON.pretty_generate(item_issues)
    end
  end

  def increment_items_scraped(item)
    @items_scraped += 1
    if verbose
      output.puts JSON.pretty_generate(item)
    else
      progress_bar&.advance(1)
    end
  end

  def record_unexpected_field(item_id, field_name)
    item_issues[item_id] ||= []
    item_issues[item_id] << "Unexpected field: #{field_name}"
    unexpected_fields << field_name
  end

  private

  attr_reader :output, :verbose, :items_scraped, :progress_bar
  attr_accessor :item_issues, :unexpected_fields
end
