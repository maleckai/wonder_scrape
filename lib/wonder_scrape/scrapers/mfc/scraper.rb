# frozen_string_literal: true

require 'upton'
require_relative 'mfc'
require_relative 'item_parser'

module WonderScrape::Scrapers::MFC
  class Scraper
    NAME = 'MFC'
    FIELDS = ItemParser::VALID_FIELD_NAMES

    BASE_URL = 'myfigurecollection.net'
    SEARCH_PATH = '/browse.v4.php'
    SEARCH_RESULT_ITEM_SELECTOR = 'ul.listing div.item-icons span.item-icon > a.tbx-tooltip'
    RESULTS_PER_PAGE = 81

    DEFAULT_DELAY_BETWEEN_REQUESTS = 2 # seconds
    DEFAULT_MAX_PAGES = 2
    DEFAULT_START_PAGE = 1
    DEFAULT_SEARCH_CATEGORY = 4 # Garage kits

    def initialize(writer, recorder, options = {})
      @writer = writer
      @recorder = recorder 
      @options = options
    end

    def scrape
      scraper.scrape(&ItemParser.parse(writer, recorder))
    end

    private

    attr_reader :writer, :recorder, :options

    def scraper
      @scraper ||= build_scraper
    end

    def build_scraper
      new_scraper = Upton::Scraper.new(
        search_url,
        SEARCH_RESULT_ITEM_SELECTOR
      )

      new_scraper.paginated = true
      new_scraper.pagination_start_index = options[:start_page] || DEFAULT_START_PAGE
      new_scraper.pagination_max_pages = options[:num_pages] || DEFAULT_MAX_PAGES
      new_scraper.verbose = options[:verbose] || false
      new_scraper.sleep_time_between_requests = options[:request_delay] || DEFAULT_DELAY_BETWEEN_REQUESTS

      new_scraper
    end

    def search_url
      URI::HTTPS.build(
        host: BASE_URL,
        path: SEARCH_PATH,
        query: build_search_query_params
      ).to_s
    end

    def build_search_query_params
      URI.encode_www_form({
                            'mode': 'search',
                            'categoryId': DEFAULT_SEARCH_CATEGORY,
                            'sort': 'date',
                            'order': 'desc'
                          })
    end
  end
end
