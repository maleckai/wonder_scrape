# frozen_string_literal: true

require_relative 'mfc'

module WonderScrape::Scrapers::MFC
  module FieldParsers
    class Standard
      def self.parse(field_content)
        field_content.text
      end
    end

    class StandardList
      def self.parse(field_content)
        field_content.search('a').map(&:text)
      end
    end

    class Price
      def self.parse(field_content)
        field_content.search('.item-price').text
      end
    end

    class Dates
      def self.parse(field_content)
        field_content.search('a.time').map(&:text)
      end
    end

    class Events
      def self.parse(field_content)
        field_content.search('a.item-entry > span').map(&:text)
      end
    end

    class MainImage
      def self.parse(field_content)
        image_url = field_content.search('#content .item-picture a.main img').attr('src')

        parsed_uri = URI.parse(image_url)
        parsed_uri.query = nil
        parsed_uri.path = parsed_uri.path.gsub('/big/', '/large/')
        parsed_uri.to_s
      end
    end

    class AdditionalImages
      STYLE_URL_REGEX = /url\(([^\(\)]+)\)/.freeze

      class << self
        def parse(field_content)
          field_content.search('#content .item-picture a.more').map do |image_link|
            extract_clean_url(image_link.attr('style'))
          end
        end

        private

        def extract_clean_url(style_string)
          image_url = style_string.scan(STYLE_URL_REGEX).flatten.first

          parsed_uri = URI.parse(image_url)
          parsed_uri.query = nil
          parsed_uri.path = parsed_uri.path.gsub('/thumbnails/', '/')
          parsed_uri.to_s
        end
      end
    end
  end
end
