# frozen_string_literal: true

require 'nokogiri'
require_relative 'mfc'
require_relative 'field_parsers'

module WonderScrape::Scrapers::MFC
  class ItemParser
    DUPLICATE_FIELD_NAMES = {
      'Artist' => 'Artists',
      'Character' => 'Characters',
      'Classification' => 'Classifications',
      'Company' => 'Companies',
      'Event' => 'Events',
      'Material' => 'Materials',
      'Origin' => 'Origins',
      'Release date' => 'Release dates'
    }.freeze

    VALID_FIELD_NAMES = [
      'Title',
      'Artists',
      'Category',
      'Characters',
      'Classifications',
      'Companies',
      'Events',
      'JAN',
      'Materials',
      'Numbering',
      'Origins',
      'Price',
      'Release dates',
      'Scale & Dimensions',
      'Various',
      'Version',
      'Images'
    ].freeze

    ID_SELECTOR = '#content #ariadne > a.current'
    TITLE_SELECTOR = '#content h1 span.headline'
    FIELD_ELEMENTS_SELECTOR = '#content .data > .form > .form-field'
    FIELD_NAME_SELECTOR = '.form-label'
    FIELD_CONTENT_SELECTOR = '.form-input'

    def self.parse(writer, recorder)
      proc do |item_html_text|
        item_html = ::Nokogiri::HTML(item_html_text)
        new(writer, recorder, item_html).parse
      end
    end

    def initialize(writer, recorder, item_html)
      @writer = writer
      @recorder = recorder
      @item_html = item_html
      @unexpected_fields = []
    end

    def parse
      result = {}
      result['Title'] = parsed_title
      result.merge! parsed_fields
      result['Images'] = parsed_images

      writer.write(result)
      recorder.increment_items_scraped(result)
    end

    private

    attr_reader :writer, :recorder, :item_html

    def parsed_id
      id_element.text
    end

    def parsed_title
      title_element.text
    end

    def parsed_fields
      fields = {}

      field_elements.each do |field_element|
        field_name = dedupe_field_name(field_name_for(field_element))

        if unexpected_field?(field_name)
          recorder.record_unexpected_field(parsed_id, field_name)
          next
        end

        field_content_element = field_content_element_for(field_element)
        field_value = case field_name
                      when 'Price'
                        FieldParsers::Price.parse(field_content_element)
                      when 'Release dates'
                        FieldParsers::Dates.parse(field_content_element)
                      when 'Events'
                        FieldParsers::Events.parse(field_content_element)
                      when 'Artists', 'Characters', 'Classifications', 'Companies', 'Materials', 'Origins'
                        FieldParsers::StandardList.parse(field_content_element)
                      else
                        FieldParsers::Standard.parse(field_content_element)
        end

        fields[field_name] = field_value
      end

      fields
    end

    def parsed_images
      images = []
      images << FieldParsers::MainImage.parse(item_html)
      images.concat FieldParsers::AdditionalImages.parse(item_html)
      images.compact.uniq
    end

    def id_element
      item_html.search(ID_SELECTOR)
    end

    def title_element
      item_html.search(TITLE_SELECTOR)
    end

    def field_elements
      item_html.search(FIELD_ELEMENTS_SELECTOR)
    end

    def field_name_for(field_element)
      field_element.search(FIELD_NAME_SELECTOR).text
    end

    def dedupe_field_name(field_name)
      DUPLICATE_FIELD_NAMES[field_name] || field_name
    end

    def field_content_element_for(field_element)
      field_element.search(FIELD_CONTENT_SELECTOR)
    end

    def unexpected_field?(field_name)
      !VALID_FIELD_NAMES.include?(field_name)
    end
  end
end
