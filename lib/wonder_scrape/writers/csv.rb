# frozen_string_literal: true

require 'csv'
require_relative 'writers'

class WonderScrape::Writers::CSV
  NAME = 'csv'

  def initialize(file_name, headers)
    @headers = headers
    @csv = build_csv_writer(file_name)
  end

  def write(entry)
    csv << entry.values_at(*headers)
  end

  def output_results
    csv.close
  end

  private

  attr_reader :headers
  attr_accessor :csv

  def build_csv_writer(file_name)
    new_csv = CSV.open(file_name, 'wb')
    new_csv << headers
    new_csv
  end
end
