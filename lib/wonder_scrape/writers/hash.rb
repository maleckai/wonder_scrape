# frozen_string_literal: true

require 'json'
require_relative 'writers'

class WonderScrape::Writers::Hash
  NAME = 'hash'

  def initialize
    @results = []
  end

  attr_reader :results

  def write(entry)
    @results << entry
  end

  def output_results
    puts JSON.pretty_generate(@results)
  end
end
