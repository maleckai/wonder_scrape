# frozen_string_literal: true

require 'tty-progressbar'

require_relative '../command'
require_relative '../scrapers/mfc/scraper'
require_relative '../writers/csv'
require_relative '../writers/hash'
require_relative '../recorder'

module WonderScrape
  module Commands
    class Scrape < WonderScrape::Command
      VALID_SCRAPER_NAMES = [
        WonderScrape::Scrapers::MFC::Scraper::NAME
      ].freeze

      VALID_WRITERS = [
        WonderScrape::Writers::CSV::NAME,
        WonderScrape::Writers::Hash::NAME
      ].freeze

      def initialize(raw_options)
        @raw_options = raw_options
      end

      def execute(input: $stdin, output: $stdout)
        recorder = WonderScrape::Recorder.new(output, options)
        writer = build_writer
        scraper = build_scraper(writer, recorder)

        scraper.scrape
        writer.output_results
        recorder.print
      end

      private

      attr_reader :raw_options

      def build_scraper(writer, recorder)
        target_module.new(writer, recorder, options)
      end

      def build_writer
        case output
        when WonderScrape::Writers::CSV::NAME
          WonderScrape::Writers::CSV.new(file, target_module::FIELDS)
        when WonderScrape::Writers::Hash::NAME
          WonderScrape::Writers::Hash.new
        end
      end

      def target_module
        @target_module ||= case target
                           when WonderScrape::Scrapers::MFC::Scraper::NAME
                             WonderScrape::Scrapers::MFC::Scraper
        end
      end

      def progress_bar
        TTY::ProgressBar.new('[:bar] :percent', total: approximate_records)
      end

      def target
        @target ||= raw_options[:target] || prompt.select('What website would you like to scrape?', VALID_SCRAPER_NAMES)
      end

      def output
        @output ||= raw_options[:format] || prompt.select('How would you like to output?', VALID_WRITERS)
      end

      def file
        @file ||= raw_options[:file] || prompt.ask('Please specify the file path you want to write to:', required: true)
      end

      def options
        @options ||= raw_options.merge({
                                         progress_bar: progress_bar,
                                         num_pages: num_pages
                                       })
      end

      def approximate_records
        target_module::RESULTS_PER_PAGE * num_pages
      end

      def num_pages
        @num_pages ||= raw_options[:num_pages] || prompt.ask('How many pages of search results do you want to scrape?', default: target_module::DEFAULT_MAX_PAGES, convert: :int)
      end
    end
  end
end
